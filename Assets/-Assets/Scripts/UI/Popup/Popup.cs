﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Doozy.Engine.UI;

public class Popup : MonoBehaviour
{
    public UIPopup mUIPopup; //두지 팝업을 가지고 온다

    //CallBack
    Action mOKCallBack;
    Action mCancleCallBack;

    //Text & Button type
    public TextMeshProUGUI mTitle_text;
    public TextMeshProUGUI mMsg_text;
    public GameObject mYesorNo_Object;
    public GameObject mOK_Object;


    //Button Type
    public enum Type {
        OnlyOk,
        YesORNo,
    }
    #region UnityEvent
    private void OnEnable()
    {
        GlobalValue.mPopupEnable = true;
    }

    private void OnDisable()
    {
        GlobalValue.mPopupEnable = false;
    }
    #endregion


    //버튼의 타입을 받습니다.
    public void setType(Type _type)
    {
        mYesorNo_Object.SetActive(false);
        mOK_Object.SetActive(false);
        switch (_type)
        {
            case Type.OnlyOk:
                mOK_Object.SetActive(true);
                break;
            case Type.YesORNo:
                mYesorNo_Object.SetActive(true);
                break;
        }
    }

    //타이틀
    public void setTitle(string _str)
    {
        mTitle_text.text = _str;
    }

    //메시지
    public void setMsg(string _str)
    {
        mMsg_text.text = _str;
    }

    //OK CallBack
    public void setOKCallBack(Action _action)
    {
        mOKCallBack = _action;
    }

    //Cancle Callback
    public void setCancleCallBack(Action _action)
    {
        mCancleCallBack = _action;
    }

    //버튼 연동
    public void OnOKButton()
    {
        mOKCallBack?.Invoke();
        mUIPopup.Hide();
    }

    //버튼 연동
    public void OnCancleButton()
    {
        mCancleCallBack?.Invoke();
        mUIPopup.Hide();
    }
}
