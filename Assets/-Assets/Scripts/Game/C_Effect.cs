﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_Effect : MonoBehaviour
{
    public List<Sprite> mSprites; //Effect sprite
    public SpriteRenderer mEffect_SpriteRenderer; //Effect 2d Renderer

    private void Awake()
    {
        mEffect_SpriteRenderer.sprite = null;

        //김성환 2021-01-13
        //이펙트에 마지막은 NULL 이였으면 좋겠습니다. 
        //이펙트가 안보이기 위함입니다.
        if (mSprites[mSprites.Count - 1] != null)
            mSprites.Add(null);
    }

    public void Effect_Play()
    {
        if (mEffectPlayCoroutine != null)
        {
            StopCoroutine(mEffectPlayCoroutine);
            mEffectPlayCoroutine = null;
        }
        if(mEffectPlayCoroutine == null)
            mEffectPlayCoroutine = StartCoroutine(@EffectPlayCoroutine());
    }

    //실행 코루틴을 제어 할수 있게 변수 선언
    Coroutine mEffectPlayCoroutine = null;
    IEnumerator @EffectPlayCoroutine()
    {
        for (int i = 0; i < mSprites.Count; i++) //이미지 수 만큼 반복
        {
            mEffect_SpriteRenderer.sprite = mSprites[i]; //이미지 변경
            yield return new WaitForSeconds(GlobalValue.mCharacter_Effect_Animation_Speed); // 대기
        }
        mEffectPlayCoroutine = null;
    }


}
