﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    public enum BLOCK_TYPE {
        NONE, // 0
        FIRE, // 1
        WATER, // 2
    }

    public BLOCK_TYPE mBLOCK_TYPE = BLOCK_TYPE.NONE;

    public GameObject mBlock_Hide;
    public GameObject mBlock1;
    public GameObject mBlock2;

    [Header("Data")]
    private bool misHide = false;

    public void setBlockType(BLOCK_TYPE _block_type)
    {
        mBlock1.SetActive(false);
        mBlock2.SetActive(false);

        switch (_block_type)
        {
            case BLOCK_TYPE.FIRE:
                mBlock1.SetActive(true);
                break;
            case BLOCK_TYPE.WATER:
                mBlock2.SetActive(true);
                break;
        }

        mBLOCK_TYPE = _block_type;
    }

    public void setHide(bool b)
    {
        mBlock_Hide.SetActive(b);

        misHide = b;
    }

    public bool isHdie()
    {
        return misHide;
    }

    
}
