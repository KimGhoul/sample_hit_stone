﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour
{
    [Header("Manager List 리스트")]
    public BlockManager mBlockManager;
    public UIManager mUIManager;

    [Header("플레이어")]
//  공개여부 c#이름(class)  선언 이름(변수)    공개여부(public , private)
    public Character mPlayer; //플레이어
    
    [Header("Data")]
    private int mScore = 0;

    public int SCORE { set { mScore = value;
            mUIManager.setScore(value);
        } get { return mScore; } }

    public float mGameTime = 0;     //현제 시간을 기록
    public float mBastGameTime = GlobalValue.mGameMaxTime; //베스트 시간을 기록
    public int mBastScore = 0;      //베스트 점수 

    public void Start()
    {
        BlockManager.mEndEvent += GameEnd;

        SaveDataLoad();

        InitData();
    }

    private void OnDisable()
    {
        BlockManager.mEndEvent -= GameEnd;
    }

    //초기화 코드
    public void InitData()
    {
        SCORE = 0;
        mGameTime = 0;
    }
    

    private void Update()
    {
        

        //Menu Step Update
        MainUpdate();
    }

    #region Updata 업데이트

    //MenyType == GameMenu 
    public void InputUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            OnLeftButtonEvent();
        }
        else if (Input.GetKeyDown(KeyCode.P))
        {
            OnRightButtonEvent();
        }

        if (Input.GetKeyDown(KeyCode.PageUp))
        {
            mGameTime += 10;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
            UIManager.ShowPopup("알림", "메인 메뉴로 돌아 가시겠습니까?", Popup.Type.YesORNo, GoToMainMenu);
    }
    

    public void MainUpdate()
    {
        switch(mMENUTYPE)
        {
            case MENUTYPE.MAINMENU: //메인 메뉴 업데이트
                MainMenuUpdate();
                break;
            case MENUTYPE.GAMEMENU: //게임 메뉴 업데이트
                InputUpdate();
                GameUpdate();
                break;
            case MENUTYPE.ENDMENU:
                break;
        }
    }
    public void MainMenuUpdate()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
            UIManager.ShowPopup("알림", "게임을 종료 하시겠습니까?", Popup.Type.YesORNo, GameExit);
    }
    public void GameExit()
    {
        Application.Quit();
    }

    public void GameUpdate()
    {
        switch(mGAMETYPE)
        {
            case GAMETYPE.SECONDS:
            case GAMETYPE.STONE:
                GameSecondsUpdate();
                break;
            case GAMETYPE.MEMORY:
                GameMemoryUpdate();
                break;
        }
    }

    public void GameSecondsUpdate()
    {
        //시간이 존재 하는 게임이니깐 완전이 로직이 변경 되어야 한다.
        if (mGameTime < GlobalValue.mGameMaxTime) //게임중
        {
            mGameTime += Time.deltaTime;
            mUIManager.setGameOverEnable(false);
            mUIManager.setTime(mGAMETYPE, mGameTime);
            mUIManager.setTimeValue(1 - (mGameTime / GlobalValue.mGameMaxTime));
        }
        else //게임 오버 100초 타임 게임일시 종료
        {
            GameEnd(mGAMETYPE, true);
        }
    }

    



    //게임 종료 일때 호출 된다.
    public void GameEnd(GAMETYPE _gameType, bool _success)
    {
        if (_success == true)
        {
            //베스트 점수 기록
            if (mBastScore < mScore)
                mBastScore = mScore;

            //베스트 시간 기록
            if (mBastGameTime > mGameTime)
                mBastGameTime = mGameTime;
            //Save
            setBastScore(mBastScore);

        }



        //EndMenu
        mUIManager.setEndMenu_GameType(_gameType);

        mUIManager.setEndMenu_BastScore(mBastScore);
        mUIManager.setEndMenu_NowScore(mScore);

        mUIManager.setEndMenu_BastTime(mBastGameTime);
        mUIManager.setEndMenu_NowTime(mGameTime);

        Doozy.Engine.GameEventMessage.SendEvent("GoTo EndMenu"); //portal 불러 오는 코드
                                                                 //EndMenu End

        //GameMenu
        //mUIManager.setGameOverEnable(true);
        mBlockManager.DrawStoneCount(GlobalValue.mBloclMax);
        mUIManager.setTime(mGAMETYPE, GlobalValue.mGameMaxTime);
        mUIManager.setTimeValue(0);
        //GameMenu End

        mMENUTYPE = MENUTYPE.ENDMENU;
    }
    #endregion

    #region Input업데이트
    //블록 타격일때 호출된다 BlockManager UnityEvent로 invoke된다.
    public void OnPlayer_Attack_Play()
    {
        if (mPlayer != null)
            mPlayer.Attack_Play();
    }

    public void OnLeftButtonEvent()
    {
        if (mNohiting == true)
            return;

        if (mGAMETYPE == GAMETYPE.MEMORY) //게임이 메모리 
        {
            if (mGAMEMEMORY_STEP == GAMEMEMORY_STEP.DRAW) //스톤을 만들고 난 이후
            {
                setGameMemory_Step(GAMEMEMORY_STEP.HIDE); //스톤을 숨긴다.
                setGameMemory_Step(GAMEMEMORY_STEP.CHECK); //스톤을 체크 한다.
            }
        }

        //블록을 잘못 눌렀을때 딜레이를 준다.
        if (mMENUTYPE == MENUTYPE.GAMEMENU)
            mBlockManager.firstBlockRemove(Block.BLOCK_TYPE.FIRE);
    }

    public void OnRightButtonEvent()
    {
        if (mNohiting == true)
            return;

        if (mGAMETYPE == GAMETYPE.MEMORY)
        {
            if (mGAMEMEMORY_STEP == GAMEMEMORY_STEP.DRAW) //스톤을 만들고 난 이후
            {
                setGameMemory_Step(GAMEMEMORY_STEP.HIDE); //스톤을 숨긴다.
                setGameMemory_Step(GAMEMEMORY_STEP.CHECK); //스톤을 체크 한다.
            }
        }
        //블록을 잘못 눌렀을때 딜레이를 준다.
        if (mMENUTYPE == MENUTYPE.GAMEMENU)
            mBlockManager.firstBlockRemove(Block.BLOCK_TYPE.WATER);
    }

    private bool mNohiting = false; 

    public void Nohiting()
    {
        if (mNohitingCoroutine != null)
        {
            StopCoroutine(mNohitingCoroutine);
            mNohitingCoroutine = null;
        }
        mNohitingCoroutine = StartCoroutine(@NohitingCoroutine());
    }

    Coroutine mNohitingCoroutine = null;
    IEnumerator @NohitingCoroutine()
    {
        mNohiting = true;
        yield return new WaitForSeconds(0.25f);
        mNohiting = false;
        mNohitingCoroutine = null;
    }

    #endregion
    public void setPlusScore(int _n)
    {
        SCORE += _n;
    }
}
