﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour //Menu
{
    public MENUTYPE mMENUTYPE = MENUTYPE.MAINMENU;

    //mainMenu가 그려지면 호출 된다.
    public void OnGoToMainMenu()
    {
        setMenuType(MENUTYPE.MAINMENU);
    }

    //gameMenu가 그려지면 호출된다.
    public void OnGoToGameMenu()
    {
        setMenuType(MENUTYPE.GAMEMENU);
    }

    //EndMenu가 그려지면 호출된다.
    public void OnGoToEndMenu()
    {
        setMenuType(MENUTYPE.ENDMENU);
    }

    public void setMenuType(MENUTYPE _menuType)
    {
        switch(_menuType)
        {
            case MENUTYPE.MAINMENU:
                InitData();
                break;
            case MENUTYPE.GAMEMENU:
                InitData();
                break;
            case MENUTYPE.ENDMENU:
                break;
        }


        mMENUTYPE = _menuType;
    }

    public void GoToMainMenu()
    {
        Doozy.Engine.GameEventMessage.SendEvent("GoTo MainMenu");
    }
}
