﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;
using Doozy.Engine.UI;

public class UIManager : MonoBehaviour
{
    [Header("GameMenu")]
    public TextMeshProUGUI mTMP_Score_Text;
    public Text mTime_Text;
    public Image mTime_Progress_Image;
    public GameObject mTMP_GameOver;

    [Header("EndMenu")]
    public GameObject mEndMenu_ScoreType_Parent; //스코어 모드의 부모 
    public GameObject mEndMenu_TimeType_Parent;  //시간 모드의 부모

    public TextMeshProUGUI mTMP_EndMenu_BastScore_Text;
    public TextMeshProUGUI mTMP_EndMenu_NowScore_Text;

    public Text mEndMenu_BastTime_text;
    public Text mEndMenu_NowTime_text;

    #region 게임 GameMenu
    #region 점수
    public void setScore(int _score)
    {
        string str = String.Format("점수 :  <color=red>{0:000}</color>", _score);
        mTMP_Score_Text.text = str;
    }
    #endregion

    #region 시간
    public void setTime(GameManager.GAMETYPE _gameType, float _time)
    {
        switch(_gameType)
        {
            case GameManager.GAMETYPE.STONE:
                mTime_Text.text = (GlobalValue.mGameMaxTime - _time).ToString("N0");
                break;
            case GameManager.GAMETYPE.SECONDS:
                string seconds_str = String.Format("{0:000}", (GlobalValue.mGameMaxTime - _time));
                mTime_Text.text = seconds_str;
                break;
        }
    }

    public void setTimeValue(float _value)  // 0 ~ 1 값을 받겠다. 시간의 프로그레스를 표현한다.
    {
        mTime_Progress_Image.fillAmount = _value;
    }

    public void setGameOverEnable(bool _enable)
    {
        mTMP_GameOver.SetActive(_enable);
    }
    #endregion
    #endregion

    #region 결과 EndMenu
    public void setEndMenu_GameType(GameManager.GAMETYPE _gameType)
    {
        mEndMenu_ScoreType_Parent.SetActive(false);
        mEndMenu_TimeType_Parent.SetActive(false);

        switch (_gameType)
        {
            case GameManager.GAMETYPE.SECONDS:
                mEndMenu_ScoreType_Parent.SetActive(true);
                break;
            case GameManager.GAMETYPE.STONE:
            case GameManager.GAMETYPE.MEMORY:
                mEndMenu_TimeType_Parent.SetActive(true);
                break;
        }
    }

    //베스트 점수 기록 함수
    public void setEndMenu_BastScore(int _bastScore)
    {
        mTMP_EndMenu_BastScore_Text.text = _bastScore.ToString();
    }

    //현제 점수 기록 함수
    public void setEndMenu_NowScore(int _nowScore)
    {
        mTMP_EndMenu_NowScore_Text.text = _nowScore.ToString();
    }

    //베스트 시간 기록 함수
    public void setEndMenu_BastTime(float _bastTime)
    {
        string str = String.Format("{0:00.00}", _bastTime);

        mEndMenu_BastTime_text.text = str;
    }
    
    //현제 시간 기록 함수
    public void setEndMenu_NowTime(float _nowTime)
    {
        string str = String.Format("{0:00.00}", _nowTime);
        mEndMenu_NowTime_text.text = str.ToString();
    }

    #endregion

    #region Popup

    public static void ShowPopup(string _title , string _msg , Popup.Type _type, Action _okCallback, Action _cancleCallBack = null)
    {
        if (GlobalValue.mPopupEnable == true)
            return;

        UIPopup uiPopup = UIPopupManager.GetPopup("PopupSystem");
        Popup popup = uiPopup.GetComponent<Popup>();
        popup.setTitle(_title);
        popup.setMsg(_msg);
        popup.setType(_type);
        popup.setOKCallBack(_okCallback);
        popup.setCancleCallBack(_cancleCallBack);

        uiPopup.Show();
    }

    #endregion


}
