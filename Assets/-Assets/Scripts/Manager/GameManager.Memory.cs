﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour //Memory
{
    public GAMEMEMORY_STEP mGAMEMEMORY_STEP = GAMEMEMORY_STEP.NONE;
    private int mMemoryCount = 2;
    public void Init_GameMemory()
    {
        mMemoryCount = 2;
        setGameMemory_Step(GAMEMEMORY_STEP.DRAW);
    }

    public void GameMemoryUpdate()
    {
        switch(mGAMEMEMORY_STEP)
        {
            case GAMEMEMORY_STEP.CHECK:     //스톤이 다 사라졌는지 체크한다.
                if (mScore == mMemoryCount)
                {
                    if (mMemoryCount < GlobalValue.mGameMemoryMaxCount)
                    {
                        setGameMemory_Step(GAMEMEMORY_STEP.DRAW);
                    }
                    else
                    {
                        GameEnd(mGAMETYPE, true);
                    }
                }
                break;
        }
    }

    public void setGameMemory_Step(GAMEMEMORY_STEP _step)
    {
        switch (_step) //초기화
        {
            case GAMEMEMORY_STEP.DRAW:  //그린다
                SCORE = 0;
                mMemoryCount++;
                mBlockManager.DrawStoneCount(mMemoryCount);
                break;
            case GAMEMEMORY_STEP.HIDE:  //스톤을 가린다
                mBlockManager.HideEnableCount(mMemoryCount);
                
                break;
            case GAMEMEMORY_STEP.CHECK: //스톤이 다 사라졌는지 체크한다.
                break;
        }

        mGAMEMEMORY_STEP = _step;
    }
}
