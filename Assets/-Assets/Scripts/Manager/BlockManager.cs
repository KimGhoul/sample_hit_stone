﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BlockManager : MonoBehaviour
{
    [Header("Manager List")]
    public GameManager mGameManager;

    public Transform mBlockParent;    //블록의 부모 자리
    public GameObject mBlockPrefab;   //생성블록 프리팹

    public List<Block> mBlocks;


    //성공일때
    public UnityEvent mAttackEvent;    //UnityEvent

    //실패일때
    //public UnityEvent<int> mFailEvent;    //UnityEvent
    public static Action<GameManager.GAMETYPE, bool> mEndEvent;


    void Start()
    {
        Vector3 pos = Vector3.zero;  // 0,0,0
        for (int i = 0; i < GlobalValue.mBloclMax; i++)
        {
            // 생성부
            GameObject obj = GameObject.Instantiate(mBlockPrefab); //블록 생성 코드
            obj.transform.parent = mBlockParent; 
            obj.transform.localPosition = pos; 
            // 정의부
            Block block = obj.GetComponent<Block>();
            if (block != null)
            {
                mBlocks.Add(block); //블록을 리스트에 추가 한다. [block] [block] [block] [block] [block] [block] [block] [block]
                block.setBlockType(BlockRandom());
                block.setHide(false); //숨기는 부분을 끈다.
            }

            // 변형부
            pos.y += 0.5f;
        }
    }

    public void DrawStoneCount(int _count)
    {
        if (mBlocks.Count == 0)
            return;

        Vector3 pos = Vector3.zero;
        for (int i = 0; i < _count; i++)
        {
            mBlocks[i].gameObject.SetActive(true);
            mBlocks[i].transform.localPosition = pos;
            pos.y += 0.5f;
        }

        for (int i = _count; i < mBlocks.Count; i++)
        {
            mBlocks[i].setHide(false);
            mBlocks[i].gameObject.SetActive(false);
        }
    }

    public void HideEnableCount(int _count)
    {
        for (int i = 0; i < _count; i++)
        {
            if(mBlocks[i].isHdie() == false)
                mBlocks[i].setHide(true);
        }
    }

    public Block.BLOCK_TYPE BlockRandom()
    {
        int random = UnityEngine.Random.Range(0, 2); // 0 ~ 1 
        return random == 0 ? Block.BLOCK_TYPE.FIRE : Block.BLOCK_TYPE.WATER;
    }

    public void firstBlockRemove(Block.BLOCK_TYPE _type)
    {
        Block block = mBlocks[0];

        //체크

        if (block.mBLOCK_TYPE != _type)
        {
            if (mGameManager.mGAMETYPE == GameManager.GAMETYPE.STONE)
            {
                mEndEvent.Invoke(mGameManager.mGAMETYPE, false);
            }
            else { //딜레이를 건다. 
                mGameManager.Nohiting();
            }
            return; //실패
        }
        //정답

        Doozy.Engine.Soundy.SoundyManager.Play("Game", "hitting");
        mAttackEvent.Invoke();

        mGameManager.setPlusScore(1);
        block.setBlockType(BlockRandom());
        mBlocks.Remove(block); //[block1] [block2] [block3] [block4] [block5]..... 
        mBlocks.Add(block);
        
        Vector3 pos = Vector3.zero;
        foreach (Block obj in mBlocks)
        {
            obj.transform.localPosition = pos;

            pos.y += 0.5f;
        }

    }


    


    
}
